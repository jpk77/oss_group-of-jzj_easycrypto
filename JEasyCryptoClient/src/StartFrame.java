import java.io.Console;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import org.json.simple.JSONObject;
import java.awt.*;
import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


 
public class StartFrame extends JFrame implements ActionListener { 
	
	
	private static final long serialVersionUID = 4765927562285786143L;

	private ResponseReader reader = null;

	private DatagramSocket socket = null;
	private InetAddress serverAddr = null;
	
	private static final int SERVER_PORT = 10000;
	private static int CLIENT_PORT = 10001;
	
	private static final int FRAME_WIDTH = 1100;
	private static final int FRAME_HEIGHT = 700;
	
	private static final int BUTTON_WIDTH = 150;
	private static final int BUTTON_HEIGHT = 20;

	private JLabel headLabel = new JLabel("Welcome to CryptoClient");
	private JLabel serverLabel = new JLabel("Enter CryptoServer address in the form \"123.123.123.123\" or hostname");
	 
	private TextField serverAdd = new TextField(30);
	 
	private JLabel portLabel = new JLabel("Enter port number for this client in the form \"10001\"");
	private TextField portNum = new TextField(30);
	 
	private JButton connectBtn = new JButton("Connect");
	
		
	private Container cont;
	
     
  
    
    public StartFrame(String title) {
        
    	super(title);
		
		
	connectBtn.addActionListener(this);
	cont = getContentPane();
	cont.setBackground(new java.awt.Color(204, 166, 166));
	cont.setFont(new Font("Arial", Font.BOLD,40));
        
	setLayout(new GridBagLayout());
	GridBagConstraints gc = new GridBagConstraints();
    
        gc.insets = new Insets(10,10,10,10);
        gc.anchor = GridBagConstraints.NORTH;
        gc.weightx = 0.5;
        gc.weighty = 0.5;
        gc.gridx = 0;
        gc.gridy = 0;
		cont.add(headLabel, gc);
        
        
        gc.gridy ++;
        gc.ipady = 40; 
        cont.add(serverLabel, gc);
        
       
        gc.gridy++;
        cont.add(serverAdd, gc);
        
		gc.gridy ++;
        cont.add(portLabel, gc);
        
      
        gc.gridy ++;
        cont.add(portNum, gc);
		
		gc.gridy ++;
        cont.add(connectBtn, gc);
		
		
	}
	
	public void actionPerformed(ActionEvent e) {

	//**********************Connect-button pressed************************
		
		
	
		if (e.getSource() == connectBtn) {
	
			
			if(serverAdd.getText().length()!=0 && portNum.getText().length()!=0) { 
			
					
				try {
					
					String address = serverAdd.getText();
					String port = portNum.getText();
					serverAddr=queryServerAddress(address,port);
					MenuFrame menu = new MenuFrame("Connected to server",serverAddr,CLIENT_PORT);
			
					menu.setSize(FRAME_WIDTH, FRAME_HEIGHT);
					menu.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
					menu.setLocationRelativeTo(null);

					menu.setVisible(true);
					this.setVisible(false);
				}
				 catch (Exception socke) {
					socke.printStackTrace();
				 }
				
				
				
			}
		}
	}
	
		private InetAddress queryServerAddress(String address, String port) {
		
		
			InetAddress addr;
			try {
				
				addr = InetAddress.getByName(address);
			} 
			catch (UnknownHostException e) {
				
				e.printStackTrace();
				addr = null;
			}
			
			CLIENT_PORT = Integer.valueOf(port).intValue();
			
			return addr;
	
		}
		
}
