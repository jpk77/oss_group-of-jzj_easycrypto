import java.io.*;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import org.json.simple.JSONObject;
import java.awt.*;
import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;



public class MenuFrame extends JFrame implements ActionListener, ReaderObserver, ItemListener  { 


	
	
	private ResponseReader reader = null;
	private DatagramSocket socket = null;
	private InetAddress serverAddr = null;
	
	private static final int SERVER_PORT = 10000;
	private static int CLIENT_PORT = 10001;
	
	private static final int FRAME_WIDTH = 800;
	private static final int FRAME_HEIGHT = 700;
	
	private static final int BORDERLAYOUT_WIDTH = 400;
	private static final int BORDER_HEIGHT = 80;
	
	private static final int LARGE_BUTTON_WIDTH = 150;
	private static final int MEDIUM_BUTTON_WIDTH = 100;
	private static final int BUTTON_HEIGHT = 20;



	private int requestId = 0;
	
	 
	private JLabel responseLabel = new JLabel("Response");
	private JLabel idLabel = new JLabel("ID");
	private JLabel operationLabel = new JLabel("Operation");
	private JLabel messageLabel = new JLabel("Result");
	private JLabel dataLabel = new JLabel("Message");
		
	 
	private String methodtype = "Reverse";
	private JLabel addressLabel;
	private JLabel cryptLabel; 
	private JTextField message = new JTextField(30);
	
	private JButton backBtn = new JButton("Back");
	private JButton cabBtn = new JButton("Cababilities");
	private JButton encryptBtn = new JButton("Encrypt");
	private JButton decryptBtn = new JButton("Decrypt");
	private JButton quitBtn = new JButton("Quit");
	
	
	private JLabel methodLabel = new JLabel( methodtype+ " method selected");
	 
	private ButtonGroup group = new ButtonGroup();
	 
	private JRadioButton rb1 = new JRadioButton("Reverse", true);
    	private JRadioButton rb2 = new JRadioButton("Matrix");
    	private JRadioButton rb3 = new JRadioButton("Cyril");

        
	 
	private JLabel nameLabel7 = new JLabel("Message");
	 
	private JTextField messageField = new JTextField(30);
	 
    private Container cont2;
	
	private JPanel serverResponse;
	
			
	 
	 
	public MenuFrame(String title, InetAddress serverAddress, int clientPort) {
			
		super(title);
		this.serverAddr=serverAddress;
		this.CLIENT_PORT=clientPort;
			
		try {	
			
			socket = new DatagramSocket(CLIENT_PORT);
			reader = new ResponseReader(socket, this);
			reader.start();
			
			addressLabel = new JLabel("Serveraddress " +serverAddr);
			cryptLabel   = new JLabel("Message to encrypt/decrypt");
			
			cabBtn.setPreferredSize(new Dimension(LARGE_BUTTON_WIDTH,BUTTON_HEIGHT));
			cabBtn.setBackground(Color.CYAN);
			
			encryptBtn.setPreferredSize(new Dimension(LARGE_BUTTON_WIDTH,BUTTON_HEIGHT));
			encryptBtn.setBackground(Color.GREEN);
			
			decryptBtn.setPreferredSize(new Dimension(LARGE_BUTTON_WIDTH,BUTTON_HEIGHT));
			decryptBtn.setBackground(Color.YELLOW);
			
			quitBtn.setPreferredSize(new Dimension(MEDIUM_BUTTON_WIDTH,BUTTON_HEIGHT));
			quitBtn.setBackground(Color.RED);
		
			

		//***************Radio buttons*************************
			
			group.add(rb1);
			group.add(rb2);
			group.add(rb3);
			
			JPanel bgroup = new JPanel();
			bgroup.setLayout(new BorderLayout());
			bgroup.add(rb1, BorderLayout.WEST);
			bgroup.add(rb2, BorderLayout.CENTER);
			bgroup.add(rb3, BorderLayout.EAST);
			
		//***************Server response fields*****************
			
			
			serverResponse = new JPanel();
			serverResponse.setLayout(new BorderLayout(WIDTH,HEIGHT));
			serverResponse.setBackground(new java.awt.Color(201,219,218));
			serverResponse.setBorder(BorderFactory.createLineBorder(Color.black));
			
			serverResponse.add(responseLabel, BorderLayout.PAGE_START);
			serverResponse.add(operationLabel, BorderLayout.LINE_START);
			serverResponse.add(idLabel, BorderLayout.CENTER);
			serverResponse.add(dataLabel, BorderLayout.PAGE_END);
			serverResponse.add(messageLabel, BorderLayout.LINE_END);
			
		//****************Action listeners**********************
			
			cabBtn.addActionListener(this);
			backBtn.addActionListener(this);
			encryptBtn.addActionListener(this);
			decryptBtn.addActionListener(this);
			quitBtn.addActionListener(this);
			rb1.addItemListener(this);
			rb2.addItemListener(this);
			rb3.addItemListener(this);
			
			
			
			cont2 = getContentPane();
			cont2.setBackground(new java.awt.Color(214,190,170));
	
			setLayout(new GridBagLayout());
			GridBagConstraints gc = new GridBagConstraints();
		
			gc.insets = new Insets(10,10,10,10);
			gc.anchor = GridBagConstraints.NORTHWEST;
			gc.weightx = 0.5;
			gc.weighty = 0.5;
			gc.gridx = 0;
			gc.gridy = 0;
			gc.ipady = 20; 
			cont2.add(backBtn, gc);
			
			gc.gridx++;
			cont2.add(addressLabel, gc);
			
			gc.gridx++;
			cont2.add(cabBtn, gc);
			
			gc.gridx++;
			gc.anchor = GridBagConstraints.NORTHEAST;
			cont2.add(bgroup, gc);
		
			gc.gridx= 0;
			gc.gridy =2;
			cont2.add(cryptLabel, gc);
			
			
			gc.gridy++;
			gc.gridwidth=2;
			gc.fill = GridBagConstraints.HORIZONTAL;
			cont2.add(message, gc);
			
			gc.gridwidth=1;
			gc.gridy ++;
			cont2.add(encryptBtn, gc);
			
			gc.gridx ++;
			cont2.add(decryptBtn, gc);
			
			gc.gridx=3;
			cont2.add(quitBtn, gc);
			
			gc.gridx=0;
			gc.gridy ++;
			gc.gridwidth=4;
			gc.gridheight=4;
			gc.fill = GridBagConstraints.VERTICAL;
			gc.fill = GridBagConstraints.HORIZONTAL;
			cont2.add(serverResponse, gc);
			
		}
		
		catch (IOException e) {
		
			e.printStackTrace();
		}
		
	}
//*********************************Radiobutton Event******************************
	
	@Override
    public void itemStateChanged(ItemEvent event) {

        	int sel = event.getStateChange();

		if (sel == ItemEvent.SELECTED) {

            		JRadioButton button = (JRadioButton) event.getSource();
            		methodtype=button.getText(); 
			
			revalidate();
			repaint();
      
        	}
    }
//**********************Button Events***********************************
	
	public void actionPerformed(ActionEvent e) {

		if (e.getSource() == cabBtn) {

			try {
			
				handleCapabilityRequest();
			
			}
			catch (Exception exe){
				System.out.println("Cabability IO error");
			}
			
		}
		
		else if (e.getSource() == encryptBtn) {
			
			try {
			
				handleEncryptRequest();
			
			}
			catch (Exception exe){
				System.out.println("Encrypt IO error");
			}
			
		}
		
		else if (e.getSource() == decryptBtn) {
			
			try {
				
				handleDecryptRequest();
			
			}
			catch (Exception exe){
				System.out.println("Decrypt IO error");
			}

			
		}
		
		else if (e.getSource() == backBtn) {
			
			StartFrame start = new StartFrame("Server Address");
			start.setSize(FRAME_WIDTH, FRAME_HEIGHT); 
			start.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			start.setVisible(true);
			start.setLocationRelativeTo(null);
			this.setVisible(false);
			
		}

		else if (e.getSource() == quitBtn) {
		
			reader.stopReading();
			System.exit(0);
		}
		
	}
	
	private void handleCapabilityRequest() throws IOException {
		
		
		JSONObject request = new JSONObject();
		request.put("id", requestId++);
		request.put("operation", "capabilities");
		String data = request.toJSONString();
		DatagramPacket packet = new DatagramPacket(data.getBytes(), data.length(), serverAddr, SERVER_PORT);
		socket.send(packet);
		
		
	}

	private void handleEncryptRequest() throws IOException {
		
		JSONObject request = new JSONObject();
		request.put("id", requestId++);
		request.put("operation", "encrypt");
		String method = methodtype;
		String text = message.getText();
		request.put("method", method);
		request.put("data", text);
		String data = request.toJSONString();
		DatagramPacket packet = new DatagramPacket(data.getBytes(), data.length(), serverAddr, SERVER_PORT);
		socket.send(packet);
	}
	
	private void handleDecryptRequest() throws IOException {
		
		JSONObject request = new JSONObject();
		request.put("id", requestId++);
		request.put("operation", "decrypt");
		String method = methodtype;
		String text = message.getText();
		request.put("method", method);
		request.put("data", text);
		String data = request.toJSONString();
		DatagramPacket packet = new DatagramPacket(data.getBytes(), data.length(), serverAddr, SERVER_PORT);
		socket.send(packet);
	}
	
	@Override
	public void handleResponse(JSONObject response) throws InterruptedException {
		
		
		String respond="Got response from reader...";
		responseLabel.setText(respond);
		
		String idstr = "ID: "+response.get("id").toString();
		idLabel.setText(idstr);
		 
		String operationstr = "Operation: "+ response.get("operation").toString();
		operationLabel.setText(operationstr);
		 
		String messagestr = "Result: "+ response.get("result").toString();
		messageLabel.setText(messagestr);
		 
		String datastr = "Data: " + response.get("data").toString();
		dataLabel.setText(datastr);
		
		revalidate();
		repaint();
	}
	
}
			
			
			
			
			
